const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
 
// Connection URL
const url = 'mongodb://localhost:27017';
 
// Database Name
const dbName = 'gitbots';
 
exports.insertDocuments = function(collectionName, insertObj, callback) {
    // Get the documents collection
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);
        // console.log("Connected successfully to server");
       
        const db = client.db(dbName);
        const collection = db.collection(collectionName);
        // Insert some documents
        collection.insertOne(insertObj, function(err, result) {
            assert.equal(err, null);
            console.log("Inserted 3 documents into the collection");
            callback(result);
        });
        client.close();
    });
}
exports.insertManyDocuments = function(collectionName, insertArr, callback) {
    // Get the documents collection
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);
        // console.log("Connected successfully to server");
       
        const db = client.db(dbName);
        const collection = db.collection(collectionName);
        // Insert some documents
        collection.insertMany(insertArr, function(err, result) {
            assert.equal(err, null);
            callback(result);
        });
        client.close();
    });
}
exports.findDocumentCount = function(collectionName, query, callback) {
    // Get the documents collection
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);
        // console.log("Connected successfully to server");
        const db = client.db(dbName);
        const collection = db.collection(collectionName);

        collection.find(query).count(function(err, result) {
            assert.equal(err, null);
            callback(result);
        });
        client.close();
    });
}
exports.findDocumentsFields = function(collectionName, query, fields, callback) {
    // Get the documents collection
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);
        // console.log("Connected successfully to server");
        const db = client.db(dbName);
        const collection = db.collection(collectionName);

        collection.find(query, fields).toArray(function (err, result) {
            assert.equal(err, null);
            callback(result);
        });
        client.close();
    });
}
exports.findDistinctDocuments = function(collectionName, field, callback) {
    // Get the documents collection
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);
        // console.log("Connected successfully to server");
        const db = client.db(dbName);
        const collection = db.collection(collectionName);

        collection.distinct(field, function (err, result) {
            assert.equal(err, null);
            callback(result);
        });
        client.close();
    });
}
