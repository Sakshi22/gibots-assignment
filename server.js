var request = require('request');
var mongo = require('./mongo.js');
var bodyParser = require('body-parser')
var express = require('express');
var app = express();
var fs = require("fs");
var ejs = require('ejs');


app.use(express.static(__dirname ));
app.set('views', __dirname);  // Set views (index.html) to root directory
app.engine('html', ejs.renderFile);     // Default for express is Jade as the rendering engine. Change that to EJS for HTML over JADE
app.use(bodyParser.json());             // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({// to support URL-encoded bodies
    extended: true
}));

app.post('/getSummary', function (req, res) {
    getNations(function(summary){
        res.send(JSON.stringify({summary}));
        res.end();
    })
})
app.get('*', (req, res) => res.redirect('index.html'));

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})


function getNations(callback){
    mongo.findDistinctDocuments("people", "nat", function(nationArr){
        getData(nationArr, function(summary){
            callback(summary);
        });
    });
}
async function getData(nationArr, callback){
    var summaryArrF = [];
    var summaryArrM = [];
    for(var i in nationArr){
        var obj = await findCountOfRange("female", nationArr[i]);
        obj["nat"] = nationArr[i];
        summaryArrF.push(obj);
        obj = await findCountOfRange("male", nationArr[i]);
        obj["nat"] = nationArr[i];
        summaryArrM.push(obj)
    }
    callback({
        "female" : summaryArrF,
        "male" : summaryArrM
    });
}
function findCountOfRange (gender, nat){
    return new Promise((resolve, reject) => {
        var obj = {};
        var query = {$and : [ { "dob.age" : {$lte : 30}} ,  {"gender" : gender}, {"nat" : nat}]};
        mongo.findDocumentCount("people", query, function(count1){
            query = {$and : [ { "dob.age" : {$gt : 30}}, { "dob.age" : {$lt : 50}},  {"gender" : gender}, {"nat" : nat}]};
            mongo.findDocumentCount("people", query, function(count2){
                query = {$and : [ { "dob.age" : {$gte : 50}},  {"gender" : gender}, {"nat" : nat}]};
                mongo.findDocumentCount("people", query, function(count3){
                    obj["0-30"] = count1;
                    obj["30-50"] = count2;
                    obj["50andAbove"] = count3;
                    resolve(obj);
                });
            });
        });
    });
}

// ********************* Insert objects ***************
var insertArr = [];
async function forloop() {
    for(var i=0; i < 100; i++){
        await requestFunc();
    }
    mongo.insertManyDocuments("people", insertArr, function(){
        console.log("inserted Document")
    });
}

function requestFunc(){
    return new Promise((resolve, reject) => {
        request('https://randomuser.me/api/', function (err, res, body) {
            body = JSON.parse(body)
            var insertObj = body.results[0]
            insertArr.push(insertObj);
            console.log("done");
            resolve();
        });
    });
}

